import pygame
import random

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED   = (255, 0, 0)

class Block(pygame.sprite.Sprite):
    """
    This class represents the ball.
    It derives from the "Sprite" class in pygame
    """
    def __init__(self, color, width, height):
        """Constructor, pass in color of block,
           and it's x and y coordinate """
        #call parent class (Sprite) constructor
        super().__init__()
        self.count = 0
        #create an image of the block, and fill it with a color
        #This could also be an image loaded from disk
        self.image = pygame.Surface([width, height])
        self.image.fill(color)
        #Fetc the rectangle object that has the dimensions of the image 
        #Update the position of this object by setting the values of 
        #rect.x and rect.y
        self.rect = self.image.get_rect()
    
    def update(self):
        """called on each frame"""
        #Move block down one pixel
        if self.count == 30:
            self.rect.y += random.randrange(0,30)
            self.rect.x += random.randrange(-10, 10)
            self.count = 0
        self.count = self.count + 1
        
        if self.rect.y > screen_height:
            self.rect.y = random.randrange(0, screen_height)
            self.rect.x = random.randrange(0, screen_width)
            all_sprites_list.remove(block)
            block_list.remove(block)
    def reinit(self):
        for i in range(50):
            #This represents a block   
            block = Block(RED, 20, 15)
            
            #Set random location for the block  
            block.rect.x = random.randrange(screen_width)
            block.rect.y = random.randrange(screen_height)
            
            #Add block to list of objects
            block_list.add(block)
            all_sprites_list.add(block)
class Player(pygame.sprite.Sprite):
    """
    This class represents the player class
    It derives from the "Sprite" class in pygame
    and allows the creation of a circular ball sprite"""
    def __init__(self):
        #call parent class (Sprite) constructor
        super().__init__()
        self.image = pygame.image.load("ship3.png").convert()
        #self.image.fill(WHITE)
        self.image.set_colorkey(WHITE)
        self.rect = self.image.get_rect()
        
        #Draw the ellipse
        #pygame.draw.ellipse(self.image, color, [0, 0, width, height])
        
class Bullet(pygame.sprite.Sprite):
    """This class represents a bullet"""
    def __init__(self):
        super().__init__()
        self.image = pygame.Surface([4, 10])
        self.image.fill(WHITE)
        self.rect = self.image.get_rect()
    def update(self):
        self.rect.y -= 3
pygame.init()
screen_width = 1024
screen_height = 768
screen = pygame.display.set_mode([1024, 768])
background = pygame.image.load('starBackground.png')
#screen.blit(background, (0,0))
#This is a list of sprites. Each block in the program is 
#added to this list
#The list is managed by a class called 'Group'
block_list = pygame.sprite.Group()

#List of each bullet
bullet_list = pygame.sprite.Group()
#This is a list of every sprite
#All blocks and the player block as well
all_sprites_list = pygame.sprite.Group()

for i in range(50):
    #This represents a block   
    block = Block(RED, 20, 15)
    
    #Set random location for the block  
    block.rect.x = random.randrange(screen_width)
    block.rect.y = random.randrange(0, 200)
    
    #Add block to list of objects
    block_list.add(block)
    all_sprites_list.add(block)

    #Create RED player block
player = Player()
all_sprites_list.add(player)
    
done = False
clock = pygame.time.Clock()
score = 0
#Main Program Loop
while not done:
    screen.blit(background, (0,0))
    
    #---Event Processing
    pos = pygame.mouse.get_pos()
    player.rect.x = pos[0]
    player.rect.y = pos[1]
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        elif event.type == pygame.MOUSEBUTTONDOWN:
            bullet = Bullet()
            bullet.rect.x = player.rect.x
            bullet.rect.y = player.rect.y
            all_sprites_list.add(bullet)
            bullet_list.add(bullet)
    #---Game Logic    
    all_sprites_list.update()
    
    for bullet in bullet_list:
        block_hit_list = pygame.sprite.spritecollide(bullet, block_list, True)
        for block in block_hit_list:
            bullet_list.remove(bullet)
            all_sprites_list.remove(bullet)
            score = score + 1
            print(score)
            block_collision = pygame.sprite.spritecollide(player, block_list, True)
            for b in block_collision:
                all_sprites_list.remove(player)
            if (score == 50):
                block.reinit()
                score = 0
        if bullet.rect.y < -10:
            bullet_list.remove(bullet)
            all_sprites_list.remove(bullet)
        
    #Draw all the sprites
    all_sprites_list.draw(screen)
    #update screen with what's been drawn
    pygame.display.flip()
    clock.tick(60)
pygame.quit()
    