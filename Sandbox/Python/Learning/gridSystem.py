import pygame, sys
from pygame.locals import *
import TileMap

BLACK = (0, 0, 0)
BROWN = (153, 76, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
WHITE = (255, 255, 255)
theClock = pygame.time.Clock()
DIRT = 0
GRASS = 1
WATER = 2
COAL = 3
cameraX = 0
cameraY = 0
TILESIZEX = 40
TILESIZEY = 40
MAPWIDTH = 40#6
MAPHEIGHT = 42#7
countX = 0
countY = 0
#Sprite buffer for camera
LOCALTILENUMBERX = 10
LOCALTILENUMBERY = 10
minlocalX = 0
minlocalY = 0
minMapX = 50 #Minimum map distance with sprite buffer
minMapY = 50
maxlocalX = LOCALTILENUMBERX*TILESIZEX - minMapX
maxlocalY = LOCALTILENUMBERY*TILESIZEY - minMapY
maxMapX = MAPWIDTH*TILESIZEX - minlocalX
maxMapY = MAPHEIGHT*TILESIZEY - minlocalY
step = 1 #number of steps the camera will move when player reaches screen edge
screensX = int((maxMapX - maxlocalX)/(TILESIZEX*step)) #The number of x direction screens the player can transverse before being at the edge of the map
screensY = int((maxMapY - maxlocalY)/(TILESIZEY*step)) #The number of screens the player can transverse before being at the edge of the map

#Global Coordinates
globalX = 0
globalY = 0
#Local coordinates
localX = 0
localY = 0

maxLocalX = LOCALTILENUMBERX*TILESIZEX
maxLocalY = LOCALTILENUMBERY*TILESIZEY

def fromLocalToGlobal(localCoordX, localCoordY):
    globalCoordX = cameraX + localCoordX
    globalCoordY = cameraY + localCoordY
    return globalCoordX,globalCoordY

def fromGlobalToLocal(globalCoordX, globalCoordY):
    localCoordX = globalCoordX - cameraX
    localCoordY = globalCoordY - cameraY
    return localCoordX,localCoordY
    


localMapWidth  = LOCALTILENUMBERX* TILESIZEX
localMapHeight = LOCALTILENUMBERY * TILESIZEY
SPRITEX = 40
SPRITEY = 40
WORLDMAPWIDTH  = MAPWIDTH*TILESIZEX 
WORLDMAPHEIGHT =  MAPHEIGHT*TILESIZEY
running = True
keystates={'up':False, 'down':False, 'left':False, 'right':False}
#screen = pygame.display.set_mode((200,200))
screen = pygame.display.set_mode((localMapWidth, localMapHeight))
player = pygame.image.load('Zelda_Front_Left.png').convert()
player.set_colorkey(WHITE)


key_order = []
index = 0
player_rect = player.get_rect()
player_rect.y = 0
player_rect.x = 0


tiles = [
            [1, 3, 0, 1, 0, 0, 1, 3, 2, 1, 3, 2, 1, 3, 2,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [2, 2, 1, 1, 2, 0, 1, 3, 2, 2, 3, 2, 1, 0, 2,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [3, 1, 2, 2, 2, 1, 3, 2, 2, 3, 3, 2, 3, 3, 2,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [1, 2, 0, 1, 3, 0, 1, 3, 3, 2, 3, 2, 1, 1, 1,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [3, 1, 2, 2, 2, 0, 3, 3, 2, 3, 3, 2, 0, 0, 2,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [1, 2, 0, 1, 1, 0, 0, 3, 1, 1, 3, 2, 1, 3, 1,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [1, 2, 0, 1, 3, 1, 1, 3, 2, 1, 3, 2, 2, 1, 2,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [3, 1, 2, 2, 2, 0, 0, 3, 1, 3, 3, 2, 1, 3, 3,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [1, 3, 0, 1, 0, 0, 1, 3, 2, 1, 3, 2, 1, 2, 2,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [1, 3, 0, 1, 0, 3, 1, 3, 2, 3, 3, 2, 3, 3, 2,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [1, 2, 0, 1, 1, 0, 0, 3, 1, 1, 3, 2, 1, 2, 0,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [1, 2, 0, 1, 3, 1, 1, 3, 2, 1, 3, 0, 1, 3, 2,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [3, 1, 2, 2, 2, 0, 0, 3, 1, 3, 2, 0, 2, 0, 1,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [1, 3, 0, 1, 0, 0, 1, 3, 2, 1, 3, 2, 1, 3, 2,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [1, 3, 0, 1, 0, 3, 1, 3, 2, 3, 1, 0, 3, 3, 2,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [1, 3, 0, 1, 0, 0, 1, 3, 2, 1, 3, 2, 1, 3, 2,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [1, 3, 0, 1, 0, 3, 1, 3, 2, 3, 3, 2, 3, 3, 2,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [1, 2, 0, 1, 1, 0, 0, 3, 1, 1, 3, 2, 1, 2, 0,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [1, 2, 0, 1, 3, 1, 1, 3, 2, 1, 3, 0, 1, 3, 2,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [1, 2, 0, 1, 1, 0, 0, 3, 1, 1, 3, 2, 1, 2, 0,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [1, 2, 0, 1, 3, 1, 1, 3, 2, 1, 3, 0, 1, 3, 2,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [1, 3, 0, 1, 0, 0, 1, 3, 2, 1, 3, 2, 1, 3, 2,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [2, 2, 1, 1, 2, 0, 1, 3, 2, 2, 3, 2, 1, 0, 2,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [3, 1, 2, 2, 2, 1, 3, 2, 2, 3, 3, 2, 3, 3, 2,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [1, 2, 0, 1, 3, 0, 1, 3, 3, 2, 3, 2, 1, 1, 1,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [3, 1, 2, 2, 2, 0, 3, 3, 2, 3, 3, 2, 0, 0, 2,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [1, 2, 0, 1, 1, 0, 0, 3, 1, 1, 3, 2, 1, 3, 1,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [1, 2, 0, 1, 3, 1, 1, 3, 2, 1, 3, 2, 2, 1, 2,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [3, 1, 2, 2, 2, 0, 0, 3, 1, 3, 3, 2, 1, 3, 3,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [1, 3, 0, 1, 0, 0, 1, 3, 2, 1, 3, 2, 1, 2, 2,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [1, 3, 0, 1, 0, 3, 1, 3, 2, 3, 3, 2, 3, 3, 2,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [1, 2, 0, 1, 1, 0, 0, 3, 1, 1, 3, 2, 1, 2, 0,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [1, 2, 0, 1, 3, 1, 1, 3, 2, 1, 3, 0, 1, 3, 2,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [3, 1, 2, 2, 2, 0, 0, 3, 1, 3, 2, 0, 2, 0, 1,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [1, 3, 0, 1, 0, 0, 1, 3, 2, 1, 3, 2, 1, 3, 2,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [1, 3, 0, 1, 0, 3, 1, 3, 2, 3, 1, 0, 3, 3, 2,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [1, 3, 0, 1, 0, 0, 1, 3, 2, 1, 3, 2, 1, 3, 2,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [1, 3, 0, 1, 0, 3, 1, 3, 2, 3, 3, 2, 3, 3, 2,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [1, 2, 0, 1, 1, 0, 0, 3, 1, 1, 3, 2, 1, 2, 0,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [1, 2, 0, 1, 3, 1, 1, 3, 2, 1, 3, 0, 1, 3, 2,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [1, 2, 0, 1, 1, 0, 0, 3, 1, 1, 3, 2, 1, 2, 0,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
            [1, 2, 0, 1, 3, 1, 1, 3, 2, 1, 3, 0, 1, 3, 2,3, 2, 1, 0, 2,1, 3, 0, 1, 0, 0, 1, 3, 2, 1],
        ] 
map = TileMap.Map()
tilemap = map.loadMap(tiles)

pygame.init()
down = False

while running:
    globalX = cameraX + player_rect.x
    globalY = cameraY + player_rect.y
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        down = event.type == pygame.KEYDOWN
            #keys = pygame.key.get_pressed()
        if (down):
            key_order.append(event.key)
    if(down):
        #print(down)
        if ( len(key_order) != 0 ):
            for queue in key_order:
                if queue == pygame.K_UP:
                    player_rect.y -= 3
                    keystates['up'] = True
                    key_order.remove(queue)
                if queue == pygame.K_DOWN:
                    keystates['down'] = True
                    player_rect.y += 3
                    key_order.remove(queue)
                if queue == pygame.K_LEFT:
                    player_rect.x -= 3
                    keystates['left'] = True
                    key_order.remove(queue)
                if queue == pygame.K_RIGHT:
                    player_rect.x += 3
                    keystates['right'] = True
                    key_order.remove(queue)
        if keystates['up']:
            player_rect.y -= 3
        if keystates['down']:
            player_rect.y += 3 
        if keystates['left']:
            player_rect.x -= 3
        if keystates['right']:
            player_rect.x += 3       
    else:   
        keystates['up']    = False
        keystates['down']  = False 
        keystates['left']  = False
        keystates['right'] = False
    
        #print("cameraX = ", cameraX)
    #Solved map drawing issue
    #Use camera units to determine which local camera matrix
    #to use to draw the scene, alternatingly increasing map in x direction 
    #requires adding cameraYunits, and increases in y directionf
    #requires adding cameraXunits
    if not down: 
        for column in range(0,LOCALTILENUMBERX):
            for row in range(0,LOCALTILENUMBERY):
                screen.blit(map.textures[tilemap[int(cameraY/TILESIZEY)+column][int(cameraX/TILESIZEX)+row]],(row*TILESIZEX, column*TILESIZEY))
        screen.blit(player, (player_rect.x, player_rect.y))  
    elif (down):
        #Is the player trying to go left
        if keystates['left']:
            #Are they near the edge of camera?
            if(player_rect.x <= minlocalX and globalX > minMapX and countX != 0):
                #Go ahead and move the camera left
                cameraX -= TILESIZEX
                for column in range(0,LOCALTILENUMBERX):
                    for row in range(0,LOCALTILENUMBERY):
                        screen.blit(map.textures[tilemap[int(cameraY/TILESIZEY)+column][int(cameraX/TILESIZEX)+row]],(row*TILESIZEX, column*TILESIZEY))
                screen.blit(player, (maxlocalX, player_rect.y))
                player_rect.x = maxlocalX
                if(screensX != 0):
                    countX -= 1
            #if they are not near the edge of the camera
            elif(player_rect.x > minlocalX):
                #player_rect.x -= 1
                for column in range(0,LOCALTILENUMBERX):
                    for row in range(0,LOCALTILENUMBERY):
                        screen.blit(map.textures[tilemap[int(cameraY/TILESIZEY)+column][int(cameraX/TILESIZEX)+row]],(row*TILESIZEX, column*TILESIZEY))
                screen.blit(player, (player_rect.x, player_rect.y))
            elif(globalX < minMapX): 
                player_rect.x = minlocalX
            else:
                print("You have reached error state: LEFT")

        elif keystates['up']:
            #Are they near the edge of camera?
            if(localY <= minlocalY and globalY > minMapY and countY != 0):
                #Go ahead and move the camera up
                cameraY -= 1*TILESIZEY
                cameraY -= 1*TILESIZEY
                for column in range(0,LOCALTILENUMBERX):
                    for row in range(0,LOCALTILENUMBERY):
                        screen.blit(map.textures[tilemap[int(cameraY/TILESIZEY)+column][int(cameraX/TILESIZEX)+row]],(row*TILESIZEX, column*TILESIZEY))
                screen.blit(player, (player_rect.x, maxlocalY))
                player_rect.y = maxlocalY
                if(screensY != 0):
                    countY -= 1
            #if they are not near the edge of the camera
            elif(player_rect.y > minlocalX):
                #player_rect.x += 1
                for column in range(0,LOCALTILENUMBERX):
                    for row in range(0,LOCALTILENUMBERY):
                        screen.blit(map.textures[tilemap[int(cameraY/TILESIZEY)+column][int(cameraX/TILESIZEX)+row]],(row*TILESIZEX, column*TILESIZEY))
                screen.blit(player, (player_rect.x, player_rect.y))
            elif(globalY < minMapY): 
                player_rect.y = minlocalY 
            else:
                print("You have reached error state: UP")
        elif keystates['down']:
            #Are they near the edge of camera? 
            if(player_rect.y >= maxlocalY and globalY < maxMapY and countY != screensY):
                #Go ahead and move the camera up
                cameraY += 1*TILESIZEY
                for column in range(0,LOCALTILENUMBERX):
                    for row in range(0,LOCALTILENUMBERY):
                        screen.blit(map.textures[tilemap[int(cameraY/TILESIZEY)+column][int(cameraX/TILESIZEX)+row]],(row*TILESIZEX, column*TILESIZEY))
                screen.blit(player, (player_rect.x, minlocalY))
                player_rect.y = minlocalY
                if(countY != screensY):
                    countY +=1
            #if they are not near the edge of the camera
            elif(player_rect.y < maxlocalY):
                #player_rect.y -= 1
                for column in range(0,LOCALTILENUMBERX):
                    for row in range(0,LOCALTILENUMBERY):
                        screen.blit(map.textures[tilemap[int(cameraY/TILESIZEY)+column][int(cameraX/TILESIZEX)+row]],(row*TILESIZEX, column*TILESIZEY))
                screen.blit(player, (player_rect.x, player_rect.y))
            elif(globalY > maxMapY): 
                player_rect.y = maxlocalY
            else:
                print("You have reached error state: DOWN")
        elif keystates['right']:
                    #Are they near the edge of camera?
            print("globalX = ", globalX, "maxlocalX = ", maxlocalX, "player_rect.x = ", player_rect.x)
            if(player_rect.x >= maxlocalX and globalX < maxMapX and countX != screensX):
                print(screensX)
                print("-----------------Entered-----------")
                #print("globalX = ", globalX, "maxlocalX = ", maxlocalX, "player_rect.x = ", player_rect.x)
                #Go ahead and move the camera right
                cameraX += 1*TILESIZEX
                for column in range(0,LOCALTILENUMBERY):
                    for row in range(0,LOCALTILENUMBERX):
                        screen.blit(map.textures[tilemap[int(cameraY/TILESIZEY)+column][int(cameraX/TILESIZEX)+row]],(row*TILESIZEX, column*TILESIZEY))
                screen.blit(player, (minlocalX, player_rect.y))
                player_rect.x = minlocalX
                if (countX != screensX):
                    countX += 1
            #if they are not near the edge of the camera
            elif(player_rect.x < maxlocalX):
                #player_rect.x -= 1
                print("Here")
                for column in range(0,LOCALTILENUMBERY):
                    for row in range(0,LOCALTILENUMBERX):
                        screen.blit(map.textures[tilemap[int(cameraY/TILESIZEY)+column][int(cameraX/TILESIZEX)+row]],(row*TILESIZEX, column*TILESIZEY))
                screen.blit(player, (player_rect.x, player_rect.y))
            elif(globalX > maxMapX): 
                player_rect.x = maxlocalX    
            else:
                print("You have reached error state: RIGHT")
    #print("player_rect.x = ", player_rect.x, "player_rect.y = ", player_rect.y)
    pygame.display.flip()
    pygame.display.update()
    theClock.tick(360)        
            