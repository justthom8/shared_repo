import pygame

class Map():
    """ 
    class: TileMap()
        Inherits: None
        Purpose: Allow user to load any sized map
    """
    def __init__(self):
        self.DIRT = 0
        self.GRASS = 1
        self.WATER = 2
        self.COAL = 3
        self.textures = {self.DIRT: pygame.image.load('dirt.png'),
                    self.GRASS: pygame.image.load('grass.png'), 
                    self.WATER: pygame.image.load('water.png'),
                    self.COAL: pygame.image.load('coal.png')}     
                    
    def loadMap(self,obj):
        """
        Function - loadMap(obj)
                 Purpose: Loads user defined map
                 Input: obj - should be a list inside a list (matrix)
                              which will be converted to the association
                              in using definitions in the init function
                    obj is a square n x n matrix
        """
        self.tilemap = []
        list = []
        count = 0
        k = 0
        for i in range(0,len(obj)):
            count = count + 1
            for j in range(0,len(obj[0]) ): 
                if (obj[i][j] == 0):
                    list.append(self.DIRT)
                elif (obj[i][j] == 1):
                    list.append(self.GRASS)
                elif (obj[i][j] == 2):
                    list.append(self.WATER)
                elif (obj[i][j] == 3):
                    list.append(self.COAL)
                k = k + 1
            self.tilemap.append(list)
            list = []    
        print("count = ",count, "k = ", len(self.tilemap))
        return self.tilemap
    
                
                    
                    