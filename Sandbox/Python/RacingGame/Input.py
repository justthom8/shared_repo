import pygame
import sys
import math
from pygame.locals import *

class Input:
    def __init__(self):
        pass
    def getEvent(self):
        self.event = pygame.event.get()
        return self.event 
    #Modifies coordinates of class object, obj
    #passed to getInput.
    def getInput(self, obj):
        for m in pygame.event.get():
            if not hasattr(m, 'key'):
                if m.type == pygame.QUIT:
                    sys.exit(0)
            else:
                down = m.type == KEYDOWN
                #print(down)
                if m.key == K_RIGHT: 
                    obj.k_right = down* -5
                elif m.key == K_LEFT: 
                    obj.k_left = down*5
                elif m.key == K_UP: 
                    obj.k_up = down* 2
                    print(obj.k_up)
                elif m.key == K_DOWN: 
                    obj.k_down = down * -2
                elif m.key == K_ESCAPE: 
                    sys.exit(0)  