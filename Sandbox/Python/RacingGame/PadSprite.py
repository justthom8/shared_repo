import pygame

class PadSprite(pygame.sprite.Sprite):
    normal = pygame.image.load('pad.png')
    hit = pygame.image.load('explode.png')
    def __init__(self, number, position):
        super(PadSprite,self).__init__() #Can also use the syntax 
                                         #super().__init__() in python 3
        self.number = number
        self.rect = pygame.Rect(self.normal.get_rect())
        self.rect.center = position
        self.image = self.normal
    def update(self, hit_list):
        if self in hit_list: 
            self.image = self.hit
            
        else: self.image = self.normal