import pygame

class Sound:
    #Constructor
    def __init__(self,sound_file):
        self.frequency=22050
        self.size=-16
        self.channels=2
        self.buffer=128
        pygame.mixer.init(self.frequency, self.size, self.channels, self.buffer) #initialize pygame sound
        self.sound = pygame.mixer.Sound(sound_file)
        
    def playSound(self):
        self.sound.play()