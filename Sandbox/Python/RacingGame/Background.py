import pygame

class Background:
    def __init__(self, backgroundFile):
        self.background = pygame.image.load(backgroundFile)
    