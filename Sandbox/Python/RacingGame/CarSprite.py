import pygame
import math

class CarSprite(pygame.sprite.Sprite):
    MAX_FORWARD_SPEED = 10
    MAX_REVERSE_SPEED = 10
    ACCELERATION = 2
    TURN_SPEED = 5
    
    def __init__(self, image, position):
        pygame.sprite.Sprite.__init__(self)
        self.src_image = pygame.image.load(image)
        self.position = position
        self.speed = self.direction = 0
        self.k_left = self.k_right = self.k_down = self.k_up = 0

    def update(self, deltat):
		#SIMULATION
        self.speed = self.speed + (self.k_up + self.k_down)
        #print("CarSprite,k_up: ",self.k_up)
        #while (self.k_up == 0 and self.k_down == 0 and self.speed is not 0):
            #self.speed = self.speed - 0.005
        if self.speed > self.MAX_FORWARD_SPEED:
            self.speed = self.MAX_FORWARD_SPEED
        if self.speed < -self.MAX_REVERSE_SPEED:
            self.speed = -self.MAX_REVERSE_SPEED
        self.direction = self.direction + (self.k_right + self.k_left)
        x,y = self.position
		
        rad = self.direction * math.pi / 180
        x = x -self.speed*math.sin(rad)
        y = y -self.speed*math.cos(rad)
		
        self.position = (x,y)
        self.image = pygame.transform.rotate(self.src_image, self.direction)
        self.rect = self.image.get_rect()
        self.rect.center = self.position
    #--------------------------------------------
    #carSound - makes whatever sound is passed to it
    #           from obj    
    #         - obj - is a Sound object which needs to be initialized
    #                 with an audio file
    #def carSound(obj):
      #  obj.playSound()
        