# INITIALIZATION
import pygame, math, sys
from pygame.locals import *
from CarSprite import *
from PadSprite import *
from Sound import *
from Background import *
from Input import *
screen = pygame.display.set_mode((1024, 768))
clock = pygame.time.Clock()

#Create Rock obstacles
pads = [
    PadSprite(1, (200, 200)),
    PadSprite(2, (800, 200)),
    PadSprite(3, (200, 600)),
    PadSprite(4, (800, 600)),
]
current_pad_number = 0
pad_group = pygame.sprite.RenderPlain(*pads)
#Create Car
rect = screen.get_rect()
car = CarSprite('car.png', rect.center)
car_group = pygame.sprite.RenderPlain(car)
done = True
Track = Background('track.png') #Load background file
explosion = Sound('explode_sound_1.wav') #Load explosion sound file
screen.blit(Track.background, (0,0))
#pygame.mixer.music.load('Daughtry - Battleships.mp3')
userInput = Input() #define Input instance
#collision = Collision()
#TO-DO-----------------------
#----------------------------
#PadSprite should update it's own behavior when collision occurs
#This means from pad = pads[0] down to seconds = 0 should
#all be contained in PadSprite. In addition, PadSprite should disappear
#from the screen rather than reappear
#- Collision should only tell you whether there was a collision
#  This will keep implementation seperate
#-CarSprite should have a damage attribute and take damage when it collides
#- CarSprite should be confined to the track, and take damage when bumping the 
# edges of it
#- Menu option
#- Additional tracks?
#- Smaller car, or larger track to accomodate the car
#- Improve driving controls 
#- Relative paths
#- Seperate files into organized folders
count = [0, 0, 0, 0]
seconds = 60
flag = 0
#pygame.mixer.music.play(0)
while done:
	#USER INPUT
    deltat = clock.tick(60)
    userInput.getInput(car)
	#RENDERING
    pad_group.clear(screen, Track.background)
    car_group.clear(screen, Track.background)
    car_group.update(deltat)
    #Below call has a problem with the pad_group list
    #See documentation for that.
    pads = pygame.sprite.spritecollide(car, pad_group, False)
    if pads:
        pad = pads[0]
        if flag:
            seconds = seconds + 1
        if pad.number and count[pad.number-1] == 0 and seconds == 60:
            print("Hit!")
            explosion.playSound()
            pad.image = pad.hit
            #pad_group.draw(screen)
            #pad.image = pad.normal
            #pygame.mixer.music.play(0)
            current_pad_number = current_pad_number + 1
            count[pad.number-1] = 1
            if flag:
                flag = 0
        elif current_pad_number == 4:
            for pad in pad_group.sprites(): 
                pad.image = pad.normal   
                count[pad.number-1] = 0
            current_pad_number = 0
            flag = 1
            seconds = 0
            
        #pad.image = pad.normal
    pad_group.draw(screen)
    car_group.draw(screen)
    pygame.display.flip()